<?php
$title = "Примеры";
require  "blocks/header.php";
?>
<?php

?>


<H1>Примеры</H1>

<style type="text/css">
    .s1 {
        padding: 5px;
        /* Поля */
        background: #E5D3BD;
        /* Цвет фона */
        border: 2px solid #E81E25;
        /* Параметры рамки */
        width: 300px;
        display: inline-block;

    }

    .s2 {
        padding: 5px;
        /* Поля */
        background: #E5D3BD;
        /* Цвет фона */
        border: 2px solid #E81E25;
        /* Параметры рамки */
        width: 300px;
        display: inline-block;

        margin-left: -4pt;


    }

    .s3 {
        padding: 5px;
        /* Поля */
        background: #E5D3BD;
        /* Цвет фона */
        border: 2px solid #eb0b12;
        /* Параметры рамки */
        width: 300px;
        display: inline-block;

        margin-left: -4pt;


    }

    .footer {
        position: absolute;
    bottom: 0;
    width: 100%;
    }

    P.line {
        border-left: 1px dotted red;
        padding: 10px;
    }
</style>

</head>

<body>

    <table border="0" align="left" width="300">
        <tr>
            <td>
                <div class="s1">
                    <p class="line"><b>1.</b>Составить алгоритм: если введенное число больше 7, то вывести “Привет”</p><br>
                    <p><b>Введите ваше число (Please input is your a number):</b>
                        <input id="numb">
                    </p>
                    <input type="submit" onclick="myFunction()" value="Отправить">
                    <input type="reset" onclick="clearBox('demo')">
                    <p id="demo"></p>
                </div>
            </td>
        </tr>
        <table border="0" align="left" width="300">
            <tr>
                <td>
                    <div class="s2">
                        <p class="line"><b>2.</b>Составить алгоритм: если введенное имя совпадает с Вячеслав,
                            то вывести “Привет, Вячеслав”, если нет, то вывести "Нет такого имени"</p>
                        <p><b>Введите ваше имя (Please input is your First name):</b>
                            <input id="numb2">
                        </p>
                        <input type="submit" onclick="myFunction2()" value="Отправить">
                        <input type="reset" onclick="clearBox('demo2')">

                        <p id="demo2"></p>
                    </div>
                </td>
            </tr>
            <table border="0" align="left" width="300">
                <tr>
                    <td>
                        <div class="s3">
                            <p class="line"><b>3.</b>Составить алгоритм: на входе есть числовой массив,
                                необходимо вывести элементы массива кратные 3</p>
                            <b>Размер массива</b>
                            <input id="numb3">
                            <input type="submit" onclick="myFunction3()" value="Отправить">
                            <p><b>Массив</b>
                                <p id="n3"></p>
                                <input id="n4">
                                <b>
                                    <p id="demo3"></p>
                                </b>

                        </div>
                    </td>
                </tr>
            </table>





            <script src="task.js"></script>
            <?php
            require "blocks/footer.php";
            ?>